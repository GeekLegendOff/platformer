package platformer.rendering;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.glu.GLU;

public class Renderer {

	public static void ortho() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		GLU.gluOrtho2D(0, Display.getWidth(), Display.getHeight(), 0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
	
	public static void quad(float x, float y, int width, int height, int xo, int yo) {
		float size = 16.0f;
		
		glColor4f(1, 1, 1, 1);
		
		glTexCoord2f((0 + xo) / size, (0 + yo) / size); glVertex2f(x, y);
		glTexCoord2f((1 + xo) / size, (0 + yo) / size); glVertex2f(x + width, y);
		glTexCoord2f((1 + xo) / size, (1 + yo) / size); glVertex2f(x + width, y + height);
		glTexCoord2f((0 + xo) / size, (1 + yo) / size); glVertex2f(x, y + height);
	}
	
	public static void renderQuad(float x, float y, int width, int height, int xo, int yo) {
		glBegin(GL_QUADS);
		quad(x, y, width, height, xo, yo);
		glEnd();
	}
	
	public static void renderEntity(float x, float y, int width, int height, float size, int xo, int yo) {
		glColor4f(1, 1, 1, 1);
		
		glBegin(GL_QUADS);
		glTexCoord2f((0 + xo) / size, (0 + yo) / size); glVertex2f(x, y);
		glTexCoord2f((1 + xo) / size, (0 + yo) / size); glVertex2f(x + width, y);
		glTexCoord2f((1 + xo) / size, (1 + yo) / size); glVertex2f(x + width, y + height);
		glTexCoord2f((0 + xo) / size, (1 + yo) / size); glVertex2f(x, y + height);
		glEnd();
	}
	
}
