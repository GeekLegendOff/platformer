package platformer.rendering.window;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class Window {

	private int width;
	private int height;
	private String title;
	private boolean fullscreen;
	private boolean resizable;
	private boolean vsync;
	
	public Window(int width, int height, String title, boolean fullscreen, boolean resizable, boolean vsync) {
		this.width = width;
		this.height = height;
		this.title = title;
		this.fullscreen = fullscreen;
		this.resizable = resizable;
		this.vsync = vsync;
		
		try {
			Display.setDisplayMode(new DisplayMode(width, height));
			Display.setTitle(title);
			Display.setFullscreen(fullscreen);
			Display.setResizable(resizable);
			Display.setVSyncEnabled(vsync);
			Display.create();
			
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isCloseRequested() {
		return Display.isCloseRequested();
	}
	
	public void update() {
		Display.update();
	}
	
	public void close() {
		Display.destroy();
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isFullscreen() {
		return fullscreen;
	}

	public void setFullscreen(boolean fullscreen) {
		this.fullscreen = fullscreen;
	}

	public boolean isResizable() {
		return resizable;
	}

	public void setResizable(boolean resizable) {
		this.resizable = resizable;
	}

	public boolean isVsync() {
		return vsync;
	}

	public void setVsync(boolean vsync) {
		this.vsync = vsync;
	}

}
