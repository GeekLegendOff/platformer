package platformer.game;

import platformer.game.levels.Level;
import platformer.rendering.Renderer;

public class Game {
	
	private Level level;
	
	public Game() {
		this.level = new Level("default_level", 64, 64);
	}

	public void init() {
		level.init();
	}
	
	public void render() {
		Renderer.ortho();
		
		level.render();
	}
	
	public void update() {
		level.update();
	}

	public Level getLevel() {
		return level;
	}
	
	public void setLevel(Level level) {
		this.level = level;
	}
	
}
