package platformer.game.animation;

public class Animation {

	private int xo, yo;
	private int direction;
	private int length;
	private boolean idle;
	private boolean walk;
	private boolean jump;

	public Animation(int length) {
		this.xo = 0;
		this.yo = 0;
		this.length = length;
		this.idle = true;
		this.walk = false;
		this.jump = false;
	}

	public void idle() {
		if (idle) {
			xo = 0;
			yo = 0;
		}
	}

	public void walk() {
		if (walk) {
			switch (direction) {
			case 0:
				xo = 2;
				yo = 2;
				break;
			case 1:
				break;
			default:
				break;
			}
		}
	}
	
	public void update() {
		if (direction >= length) {
			direction--;
		}
		if (walk) {
			switch (direction) {
			case 0:
				xo--;
				break;
			case 1:
				xo++;
				break;
			default:
				break;
			}
		}
	}

	public int getXo() {
		return xo;
	}

	public void setXo(int xo) {
		this.xo = xo;
	}

	public int getYo() {
		return yo;
	}

	public void setYo(int yo) {
		this.yo = yo;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isIdle() {
		return idle;
	}

	public void setIdle(boolean idle) {
		this.idle = idle;
	}

	public boolean isWalk() {
		return walk;
	}

	public void setWalk(boolean walk) {
		this.walk = walk;
	}

	public boolean isJump() {
		return jump;
	}

	public void setJump(boolean jump) {
		this.jump = jump;
	}

}
