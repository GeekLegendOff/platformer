package platformer.game.levels.tiles.grass;

import platformer.game.levels.tiles.Tile;
import platformer.rendering.Renderer;

public class GrassTile extends Tile {

	public GrassTile(float x, float y) {
		super(x, y);
	}

	@Override
	public void render() {
		Renderer.quad(x, y, 16, 16, 0, 0);
	}

}
