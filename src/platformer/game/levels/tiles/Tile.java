package platformer.game.levels.tiles;

public abstract class Tile {

	public float x, y;
	public boolean block;
	public boolean vegetation;

	public Tile(float x, float y) {
		this.x = x;
		this.y = y;
		this.block = false;
		this.vegetation = false;
	}

	public abstract void render();

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public boolean isBlock() {
		return block;
	}
	
	public void setBlock(boolean block) {
		this.block = block;
	}

	public boolean isVegetation() {
		return vegetation;
	}

	public void setVegetation(boolean vegetation) {
		this.vegetation = vegetation;
	}

}
