package platformer.game.levels;

import static org.lwjgl.opengl.GL11.*;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import platformer.game.entities.manager.EntityManager;
import platformer.game.entities.player.Player;

public class Level {
	
	private String name;
	private int width;
	private int height;
	
	private EntityManager entityManager;
	private Player player;
	
	public Level(String name, int width, int height) {
		this.name = name;
		this.width = width;
		this.height = height;
		this.entityManager = new EntityManager();
		this.player = new Player(50, 50, 128, 128);

//		load();
	}
	
	public void init() {
		entityManager.add(player);
	}

	public void render() {
		glClearColor(1, 0, 0, 1);
		
		entityManager.render();
	}
	
	public void update() {
		entityManager.update();
	}
	
	private void load() {
		int[] pixels;
		BufferedImage image = null;
		try {
			image = ImageIO.read(Level.class.getResource("/levels/" + name + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		width = image.getWidth();
		height = image.getHeight();
		
		pixels = new int[width * height];
		image.getRGB(0, 0, width, height, pixels, 0, width);
		
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				
			}
		}
	}
	

}
