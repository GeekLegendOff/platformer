package platformer.game.entities.manager;

import java.util.ArrayList;
import java.util.List;

import platformer.game.entities.Entity;

public class EntityManager {
	
	private List<Entity> entities;
	
	public EntityManager() {
		this.entities = new ArrayList<Entity>();
	}
	
	public void add(Entity entity) {
		if (!contains(entity)) {
			entities.add(entity);
		}
	}
	
	public void remove(Entity entity) {
		if (contains(entity)) {
			entities.remove(entity);
		}
	}
	
	public void render() {
		for (Entity entity : entities) {
			entity.render();
		}
	}
	
	public void update() {
		for (Entity entity : entities) {
			entity.update();
		}
	}
	
	public boolean contains(Entity entity) {
		return entities.contains(entity);
	}
	
	public List<Entity> getEntities() {
		return entities;
	}

}
