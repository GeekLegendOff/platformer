package platformer.game.entities.player;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.input.Keyboard;

import platformer.game.animation.Animation;
import platformer.game.entities.Entity;
import platformer.rendering.Renderer;
import platformer.rendering.texture.Texture;

public class Player extends Entity {

	private Animation animation;
	private float moveSpeed;
	
	public Player(float x, float y, int width, int height) {
		super(x, y, width, height);
		this.animation = new Animation(3);
		this.moveSpeed = 0.5f;
	}

	@Override
	public void render() {
		Texture.player.bind();
		Renderer.renderEntity(x, y, width, height, 16.0f, animation.getXo(), animation.getYo());
		Texture.player.unbind();
		
		glTranslatef(x, y, 0);
	}

	@Override
	public void update() {
		animation.update();
		
		if (Keyboard.isKeyDown(Keyboard.KEY_Q) || Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
			animation.setIdle(false);
			animation.setWalk(true);
			animation.setDirection(1);
			animation.walk();
			x -= moveSpeed;
		} else {
			animation.setIdle(true);
			animation.setWalk(false);
			animation.idle();
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_D) || Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
			animation.setIdle(false);
			animation.setWalk(true);
			animation.setDirection(0);
			animation.walk();
			x += moveSpeed;
		} else {
			animation.setIdle(true);
			animation.setWalk(false);
			animation.idle();
		}
	}

}
