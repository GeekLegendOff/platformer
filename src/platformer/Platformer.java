package platformer;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.Display;

import platformer.game.Game;
import platformer.rendering.window.Window;

public class Platformer {
	
	private static Platformer instance;
	
	private Window window;
	
	private Game game;
	
	public Platformer() {
		this.window = new Window(800, 600, "Platformer", false, false, false);
		this.game = new Game();
	}
	
	private void start() {
		instance = this;
		game.init();
		loop();
	}
	
	public void stop() {
		instance = null;
		window.close();
		System.exit(0);
	}
	
	private void loop() {
		long before = System.nanoTime();
		long timer = System.currentTimeMillis();
		
		double ns = 1000000000.0D / 60.0D;
		
		int ups = 0;
		int fps = 0;
		
		while (!window.isCloseRequested()) {
			long now = System.nanoTime();

			if (now - before > ns) {
				before += ns;
				ups++;
				update();
			} else {
				fps++;
				render();
				window.update();
			}
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				Display.setTitle("Platformer - UPS: " + ups + " FPS: " + fps);
				ups = 0;
				fps = 0;
			}
		}
		stop();
	}
	
	private void render() {
		glClear(GL_COLOR_BUFFER_BIT);
		
		game.render();
	}
	
	private void update() {
		game.update();
	}
	
	public static void main(String[] args) {
		new Platformer().start();
	}
	
	public static Platformer getInstance() {
		return instance;
	}
	
	public Window getWindow() {
		return window;
	}
	
	public Game getGame() {
		return game;
	}
	
}
